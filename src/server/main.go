package main

import (
	"context"
	"log"
	"net"
	pb "proto"
	"google.golang.org/grpc"
)

type server struct{}

func main() {
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterAdditionServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err )
	}
}

func (s *server) Add(ctx context.Context, in *pb.Input) (*pb.Output, error) {
	var sumVal = in.First + in.Second
	log.Printf("Sum: %v", sumVal)
	return &pb.Output{Result: sumVal}, nil
}

func (s *server) MultipleSum(in *pb.Range, stream pb.Addition_MultipleSumServer) error {
	var streamSum int32 = 0
	for i := in.Begin; i <= in.End; i++ {
		streamSum += i
		err := stream.Send(&pb.Output{Result:streamSum})
		if err != nil {
			return err
		}

	}
	return nil
}

/*func (s *server) Stream(ctx context.Context, request *proto.Request) (*proto.Response, error) {
	stream, err := c.MultipleSum(context.Background(), &pb.Range{Begin: begin, End: end})
	if err != nil {
		log.Fatalf("Error on Add: %v", err)
	}
	for {
		sum, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("%v.Add(_) = _, %v", c, err)
		}
		log.Print("Sum: ", sum)
	}
}*/
